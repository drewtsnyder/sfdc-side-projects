# Salesforce Side Projects
I'll be working on a number of side projects for salesforce.com. This will be the master repo for them. It will contain all of the metadata for my development sandbox regardless of the features.

I'm going to attempt to maintain separate repos for specific features that only contain the metadata for the related feature.

## Feature Specific Repos
**STUB**

# Resources
This project used [DMC](https://github.com/kevinohara80/dmc) by [Kevin O'Hara](https://github.com/kevinohara80).

Since Kevin doesn't make it really clear how to login to your org for the first time I'm going to put it here... mainly so I don't forget.

1. `dmc init`
2. `dmc config:set default_org=yourOrgNickName`
3. `dmc login yourOrgNickName`

The `yourOrgNickName` is just a short nickname you would like to used for the org. Since DMC stores the login info outside of the project you can reference this org later if you want to spin up a new project or deploy across environments.
